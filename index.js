const express = require('express');
const predictWithLibrary = require('./predictWithLibrary');
const arimaPrediction = require('./arimaPrediction');
const app = express();
const port = 3000;

const findParameterValue = (params, paramName) => {
    let paramValue = null;
  
    for (const param of params) {
      if (param.name === paramName) {
        paramValue = param.value;
        break;
      }
    }
  
    return paramValue;
};

validateInputData = (data, params) => {
    if (!Array.isArray(data) || data.length === 0) {
        throw new Error('Neplatná vstupní data. Očekává se pole s neprázdnými daty.');
    }
    if (!Array.isArray(params) || params.length === 0) {
        throw new Error('Neplatné vstupní parametry. Očekává se pole s neprázdnými daty.');
    }
}

app.use(express.json());

app.post('/predict', (req, res) => {
    const { data, params } = req.body;

    try {
        validateInputData(data, params);
    } catch (error) {
        res.status(400).json({ error: error.message});
        return;
    }
    
    const alpha = findParameterValue(params, 'alpha');
    const useLibrary = findParameterValue(params, 'useLibrary');
    
    const values = data.map(entry => entry.value);
    let predictedValues;

    if (useLibrary) {
        predictedValues = predictWithLibrary(values);
    } else {
        predictedValues = arimaPrediction(values, alpha);
    }

    const outputData = [];
    const lastIndex = data.length - 1;

    for (let i = 0; i < predictedValues.length; i++) {
        const timestamp = new Date(data[lastIndex].timestamp);
        timestamp.setDate(timestamp.getDate() + 7 * (i + 1));
        const entry = {
            timestamp: timestamp.toISOString(),
            value: predictedValues[i]
        };
        outputData.push(entry);
    }

    for (let i = lastIndex; i >= 0; i--) {
        const timestamp = new Date(data[i].timestamp);
        const entry = {
          timestamp: timestamp.toISOString(),
          value: data[i].value
        };
        outputData.unshift(entry);
      }

    res.json({ data: outputData});
});

const server = app.listen(port, () => {
    console.log(`Server běží na portu ${port}.`);
});

server.on('error', (error) => {
    if (error.code === "EADDRINUSE") {
        console.error(`Port ${port} je již obsazen jinou aplikací.`);
    } else {
        console.error('Nastala chyba při spuštění aplikace:', error);
    }
});