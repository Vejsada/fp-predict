function exponentialSmoothing(data, alpha) {
    const result = [];
    result.push(data[0]);
  
    for (let i = 1; i < data.length; i++) {
      const predictedValue = alpha * data[i] + (1 - alpha) * result[i - 1];
      result.push(predictedValue);
    }
  
    return result;
}

function arimaPrediction(data, alpha) {  
    const smoothedValues = exponentialSmoothing(data, alpha);
  
    const predictedValues = [];
    const lastValue = smoothedValues[smoothedValues.length - 1];
    for (let i = 0; i < 5; i++) {
      const predictedValue = alpha * lastValue + (1 - alpha) * smoothedValues[smoothedValues.length - 1 - i];
      predictedValues.unshift(predictedValue);
    }
  
    return predictedValues;
}

module.exports = arimaPrediction;