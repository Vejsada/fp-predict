const Arima = require('arima');
const arima = new Arima({ p: 1, d: 0, q: 1});

function predictWithLibrary(values) {
    arima.train(values);
    const predictedValues = arima.predict(5);
    return predictedValues[0];
}

module.exports = predictWithLibrary;