# Vstupní úkol pro společnost FreshPoint

## Zvolený algoritmus

### Exponenciální vyhlazování
Exponenciální vyhlazování je jednoduchá metoda predikce, která je založena na průměrování vážených hodnot předchozích pozorování. Tato metoda je základním kamenem pro ARIMA a poskytuje dobrý základ pro predikci časových řad.

### ARIMA (AutoRegressive Integrated Moving Average)
ARIMA je statistická metoda pro analýzu a predikci časových řad. Tento algoritmus kombinuje tři základní komponenty: autoregresi (AR), integraci (I) a pohyblivý průměr (MA). ARIMA modeluje trend, sezónnost a náhodnou složku časové řady.

## Proč právě tento algoritmus?

### Široká použitelnost a možnost naprogramování
ARIMA je velmi široce používaný algoritmus pro predikci časových řad, a to v různých oblastech, jako je ekonomie, finance, meteorologie, analýza trhu a další. Má silnou statistickou a matematickou základnu, která umožňuje efektivně modelovat a predikovat různé typy časových řad.
Další z důvodů, proč jsem si vybral algoritmus ARIMA, je jeho komplexnost a současně možnost se mu přiblížit pomocí mnou naprogramovaných funkcí.

## O aplikaci

### Dependencies
- Express – jednoduchý a efektivní způsob vytváření API v Node.js
- Arima – nabízí implementaci algoritmu ARIMA

### Funkčnost
API je spustitelné pomocí Node.js na portu 3000. Požadavky jsou přijímány metodou POST na endpointu /predict ve formátu JSON obsahujícím parametry a vstupní data. Projekt je vybaven základním ošetřením portu a ověřením vstupních dat. Uživatel má možnost pomocí parametru "useLibrary" (Boolean) vybrat, zda chce použít knihovnu ARIMA nebo mou implementace pro predikci výsledků. V odpovědi na požadavek jsou zahrnuta původní data a následujících 5 týdnů predikovaných zvoleným způsobem.

## Testování
Pro testování API jsem využil nástroje Postman a vstupní data jsou k dispozici přímo v projektu ve složce testData pro snadnou reprodukovatelnost a ověření funkcionality.